﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Excel
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isInsertMode = false;
        bool isBeingEdited = false;
        public MainWindow()
        {
            InitializeComponent();
            //this.Loaded += MainWindow_Loaded;
            ModelContext context = new ModelContext();
            var result = from product in context.Products
                         select product;
            DataGrid.ItemsSource = result.ToList();
        }

        //private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        //{
        //    ModelContext context = new ModelContext();
        //    var result = from product in context.Products
        //                 select product;
        //    DataGrid.ItemsSource = result.ToList();
        //}

        //private void dgRawData_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        //{
        //    if (e.EditAction == DataGridEditAction.Commit)
        //    {
        //        ModelContext context = new ModelContext();
                
        //    }
        //}
        private ObservableCollection<Product> GetList()
        {
            using (var context = new ModelContext())
            {
                var list = from e in context.Products select e;
                return new ObservableCollection<Product>(list);
            }
        }
        private void dgEmp_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            using (var context = new ModelContext()) {
                List<Product> prod = context.Products.ToList();
                //ModelContext context = new ModelContext();
                Product product = new Product();
                Product emp = e.Row.DataContext as Product;
                
                //product.Id = emp.Id;
                //product.Amount = emp.Amount;
                //product.Cost = emp.Cost;
                //product.Name = emp.Name;
                
                //context.Products.Remove(emp);
                //context.Products.Add(product);

                if (isInsertMode)
                {
                    var InsertRecord = MessageBox.Show("Do you want to add " + emp.Name + " as a new ?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (InsertRecord == MessageBoxResult.Yes)
                    {
                        product.Id = emp.Id;
                        product.Amount = emp.Amount;
                        product.Cost = emp.Cost;
                        product.Name = emp.Name;
                        context.Products.Add(product);
                        
                        
                        DataGrid.ItemsSource = GetList();
                        context.SaveChanges();

                        
                    }
                    else
                    {
                        DataGrid.ItemsSource = GetList();
                        context.SaveChanges();
                    }

                }
                context.SaveChanges();
            }
        }

        private void dgEmp_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            using (var context = new ModelContext())
            {
                if (e.Key == Key.Delete && !isBeingEdited)
                {
                    var grid = (DataGrid)sender;
                    if (grid.SelectedItems.Count > 0)
                    {
                        var Res = MessageBox.Show("Are you sure you want to delete " + grid.SelectedItems.Count + " prod?", "Deleting Records", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (Res == MessageBoxResult.Yes)
                        {
                            foreach (var row in grid.SelectedItems)
                            {
                               
                                Product employee = row as Product;
                                context.Entry(employee).State = EntityState.Deleted;
                                context.Products.Remove(employee);
                            }
                            context.SaveChanges();
                            MessageBox.Show(grid.SelectedItems.Count + " prod have being deleted!");
                        }
                        else
                        {
                            DataGrid.ItemsSource = GetList();
                            context.SaveChanges();
                        }
                            
                    }
                }
                context.SaveChanges();
            }
            
        }

        private void dgEmp_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            isInsertMode = true;
        }

        private void dgEmp_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            isBeingEdited = true;
        }
    }
  

   

  

    
}

